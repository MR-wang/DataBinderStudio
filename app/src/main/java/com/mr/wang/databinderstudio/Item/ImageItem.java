package com.mr.wang.databinderstudio.Item;

import com.mr.wang.databinderstudio.R;

import java.util.Random;

/**
 * User: chengwangyong(chengwangyong@blinnnk.com)
 * Date: 2017-03-02
 * Time: 22:34
 */
public class ImageItem extends BaseItem {

  @Override
  public int getType() {
    return R.layout.item_image;
  }

  public final String url;
  private boolean liked;

  public ImageItem() {
    url = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1488565220707&di=02f96f35b0fdb7791634d0398b2d3033&imgtype=0&src=http%3A%2F%2Fupload.art.ifeng.com%2F2015%2F0811%2F1439260959533.jpg";
    liked = new Random().nextBoolean();
  }

  public boolean isLiked() {
    return liked;
  }

  public void toggleLiked() {
    liked = !liked;
  }
}
