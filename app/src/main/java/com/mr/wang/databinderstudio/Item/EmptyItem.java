package com.mr.wang.databinderstudio.Item;

import com.mr.wang.databinderstudio.R;
import com.mr.wang.databinderstudio.Adatper.MultiTypeAdapter;

/**
 * User: chengwangyong(chengwangyong@blinnnk.com)
 * Date: 2017-03-03
 * Time: 22:45
 */
public class EmptyItem implements MultiTypeAdapter.IItem {
  @Override
  public int getType() {
    return R.layout.item_empty;
  }

  /////////////////////////////////////////////////
  public EmptyItem() {
    content = "Currently has no items, please refresh it later!";
  }

  /////////////////////////////////////////////////
  // data part
  private final String content;

  public String getContent() {
    return content;
  }
}
