package com.mr.wang.databinderstudio.Adatper;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * User: chengwangyong(chengwangyong@blinnnk.com)
 * Date: 2017-03-02
 * Time: 23:05
 */
public class BindingUtil {
  @BindingAdapter({"imageUrl", "error", "placeholder"})
  public static void loadImage(ImageView imgView,
                               String url,
                               Drawable error,
                               Drawable placeholder) {
    Glide.with(imgView.getContext())
      .load(url)
      .error(error)
      .placeholder(placeholder)
      .into(imgView);
  }
}  
