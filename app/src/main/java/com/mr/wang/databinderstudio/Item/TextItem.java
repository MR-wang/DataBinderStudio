package com.mr.wang.databinderstudio.Item;

import com.mr.wang.databinderstudio.R;

import java.util.Date;
import java.util.Random;

/**
 * User: chengwangyong(chengwangyong@blinnnk.com)
 * Date: 2017-03-02
 * Time: 22:34
 */
public class TextItem extends BaseItem {

  @Override
  public int getType() {
    return R.layout.item_text;
  }

  public final String content;
  private boolean liked;

  public TextItem() {
    content = new Date().toString();
    liked = new Random().nextBoolean();
  }

  public boolean isLiked() {
    return liked;
  }

  public void toggleLiked() {
    liked = !liked;
  }
}
