package com.mr.wang.databinderstudio.Adatper;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mr.wang.databinderstudio.BR;

import java.util.ArrayList;
import java.util.List;

/**
 * User: chengwangyong(chengwangyong@blinnnk.com)
 * Date: 2017-03-02
 * Time: 22:09
 */
public class MultiTypeAdapter extends RecyclerView.Adapter<MultiTypeAdapter.ItemViewHolder> {


  @Override
  public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
      viewType, parent, false);
    return new ItemViewHolder(binding);
  }

  @Override
  public void onBindViewHolder(ItemViewHolder holder, int position) {
    holder.bindTo(items.get(position));
  }

  @Override
  public int getItemCount() {
    return items.size();
  }

  @Override
  public int getItemViewType(int position) {
    return items.get(position).getType();
  }

  public interface IItem {
    int getType();
  }

  class ItemViewHolder extends RecyclerView.ViewHolder {
    private final ViewDataBinding binding;

    ItemViewHolder(ViewDataBinding binding) {
      super(binding.getRoot());
      this.binding = binding;
    }

    void bindTo(MultiTypeAdapter.IItem item) {
      binding.setVariable(BR.item, item);
      binding.executePendingBindings();
    }
  }

  private List<IItem> items = new ArrayList<>();

  public List<IItem> getItems() {
    return items;
  }

  public void setItem(IItem item) {
    clearItems();
    addItem(item);
  }

  public void setItems(List<IItem> items) {
    clearItems();
    addItems(items);
  }

  public void addItem(IItem item) {
    items.add(item);
  }

  public void addItem(IItem item, int index) {
    items.add(index, item);
  }

  public void addItems(List<IItem> items) {
    this.items.addAll(items);
  }

  public void removeItem(IItem item) {
    items.remove(item);
  }

  public void clearItems() {
    items.clear();
  }
}
