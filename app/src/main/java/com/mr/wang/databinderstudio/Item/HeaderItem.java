package com.mr.wang.databinderstudio.Item;

import com.mr.wang.databinderstudio.Adatper.MultiTypeAdapter;
import com.mr.wang.databinderstudio.R;

/**
 * User: chengwangyong(chengwangyong@blinnnk.com)
 * Date: 2017-03-02
 * Time: 22:15
 */
public class HeaderItem implements MultiTypeAdapter.IItem {
  @Override
  public int getType() {
    return R.layout.item_header;
  }
}