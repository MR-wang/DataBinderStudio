package com.mr.wang.databinderstudio;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mr.wang.databinderstudio.Adatper.MultiTypeAdapter;
import com.mr.wang.databinderstudio.Item.EmptyItem;
import com.mr.wang.databinderstudio.Item.ErrorItem;
import com.mr.wang.databinderstudio.Item.FooterItem;
import com.mr.wang.databinderstudio.Item.HeaderItem;
import com.mr.wang.databinderstudio.Item.ImageItem;
import com.mr.wang.databinderstudio.Item.TextItem;
import com.mr.wang.databinderstudio.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.Random;

/**
 * 用最新的dataBinder框架构建adapter
 * http://www.jcodecraeer.com/a/anzhuokaifa/androidkaifa/2017/0219/7131.html
 */
public class MainActivity extends AppCompatActivity {

  private MultiTypeAdapter adapter = new MultiTypeAdapter();
  private SwipeRefreshLayout swipeRefreshLayout;
  private RecyclerView recyclerView;
  private HeaderItem headerItem;
  private EmptyItem emptyItem;
  private ErrorItem errorItem;
  private FooterItem footerItem;
  private boolean refreshing;
  private static final int PER_PAGE_COUNT=50;
  private boolean hasMoreData;
  private boolean loading;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ActivityMainBinding binding =
        DataBindingUtil.setContentView(this, R.layout.activity_main);

    swipeRefreshLayout = binding.swipeRefreshLayout;
    recyclerView = binding.recyclerView;
    initViews();
    initItems();
  }

  private void initItems() {
    headerItem = new HeaderItem();
    emptyItem = new EmptyItem();
    errorItem = new ErrorItem();
    footerItem = new FooterItem();
//    adapter.addItem(headerItem);
//    adapter.addItem(emptyItem);
    iItems.add(headerItem);
    iItems.add(emptyItem);
  }

  private void initViews() {
    swipeRefreshLayout.setOnRefreshListener(
        new SwipeRefreshLayout.OnRefreshListener() {
          @Override
          public void onRefresh() {
            refreshData();
          }
        });

    recyclerView.setHasFixedSize(true);
    final LinearLayoutManager llm = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(llm);
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        if (newState == RecyclerView.SCROLL_STATE_IDLE &&
            llm.findLastVisibleItemPosition() >= adapter.getItemCount() - 1) {
          loadMoreData();
        }
      }
    });
    recyclerView.setAdapter(adapter);
  }

  private void loadMoreData() {
    if (hasMoreData &&
      !loading &&
      // here the threshold depends on your actual situation
      // adapter.getItemCount() > PER_PAGE_COUNT
      adapter.getItemCount() > 10) {
      loading = true;
      fetchData(true);
    }
  }

  private void refreshData() {
    if (!refreshing) {
      refreshing = true;
      hasMoreData = true;
      swipeRefreshLayout.setRefreshing(true);
      // remove all other items, just keep headerItem
      adapter.setItem(headerItem);
      fetchData(false);
    }
  }
  public boolean loadMore;

  private void fetchData(boolean b) {
    new Handler().postDelayed(new Runnable() {


      @Override
      public void run() {
        if (refreshing) {
          refreshing = false;
          swipeRefreshLayout.setRefreshing(false);
        }
        if (loadMore) {
          loading = false;
          adapter.removeItem(footerItem);
        }
        retrieveItems(loadMore);
        adapter.notifyDataSetChanged();
      }
    }, 2000);
  }

  private void retrieveItems(boolean loadMore) {
    // result = 0, network error
    // result = 1, empty or last page data
    // result = 2 and other, normal result
    int resultType = (new Random()).nextInt(100) % 4;
    if (resultType == 0) {
      adapter.addItem(loadMore ? footerItem.setState(FooterItem.ERROR) : errorItem);
    } else if (resultType == 1) {
      if (loadMore) {
        hasMoreData = false;
        addDataItems(PER_PAGE_COUNT / 2);
        // here depends whether you want to display no more data state
        // if you don't want to display this state when has no more data
        // then just don't add it back
        adapter.addItem(footerItem.setState(FooterItem.NO_MORE));
      } else {
        adapter.addItem(emptyItem);
      }
    } else {
      addDataItems(PER_PAGE_COUNT);
      // pre-display loading state to improve user experience
      adapter.addItem(footerItem.setState(FooterItem.LOADING));
    }
  }

  private void addDataItems(int count) {
    for (int i = 0; i < count; i++) {
      adapter.addItem(i % 2 == 0 ? new ImageItem() : new TextItem());
    }
  }

  protected ArrayList<MultiTypeAdapter.IItem> iItems = new ArrayList<>();

//  private void loadData() {
//    for (int i = 0; i < 100; i++) {
//      if (i % 2 == 0) {
//        iItems.add(new TextItem());
//      }else{
//        iItems.add(new ImageItem());
//      }
//    }
//    adapter.setItems(iItems);
//    adapter.notifyDataSetChanged();
//    swipeRefreshLayout.setRefreshing(false);
//  }

}
