package com.mr.wang.databinderstudio.Item;

import com.mr.wang.databinderstudio.Adatper.MultiTypeAdapter;
import com.mr.wang.databinderstudio.R;

/**
 * User: chengwangyong(chengwangyong@blinnnk.com)
 * Date: 2017-03-03
 * Time: 23:05
 */
public class ErrorItem implements MultiTypeAdapter.IItem {
  @Override
  public int getType() {
    return R.layout.item_error;
  }

  /////////////////////////////////////////////////
  public ErrorItem() {
    content = "Error happened! please try it later!";
  }

  /////////////////////////////////////////////////
  // data part
  private String content;

  public String getContent() {
    return content;
  }
}