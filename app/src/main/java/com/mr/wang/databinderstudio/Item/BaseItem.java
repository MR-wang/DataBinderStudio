package com.mr.wang.databinderstudio.Item;

import android.view.View;

import com.mr.wang.databinderstudio.Adatper.MultiTypeAdapter;

/**
 * User: chengwangyong(chengwangyong@blinnnk.com)
 * Date: 2017-03-03
 * Time: 23:38
 */
public abstract class BaseItem implements MultiTypeAdapter.IItem {
  ////////////////////////////////////////////
  // handle event
  private View.OnClickListener onClickListener;

  public View.OnClickListener getOnClickListener() {
    return onClickListener;
  }

  public void setOnClickListener(View.OnClickListener onClickListener) {
    this.onClickListener = onClickListener;
  }
}
