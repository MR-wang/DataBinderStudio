package com.mr.wang.databinderstudio.Item;

import com.mr.wang.databinderstudio.Adatper.MultiTypeAdapter;
import com.mr.wang.databinderstudio.R;

/**
 * User: chengwangyong(chengwangyong@blinnnk.com)
 * Date: 2017-03-02
 * Time: 22:15
 */
public class FooterItem implements MultiTypeAdapter.IItem {
  @Override
  public int getType() {
    return R.layout.item_footer;
  }

  ///////////////////////////////////////////////
  public FooterItem setState(int state) {
    this.state = state;
    return this;
  }

  ///////////////////////////////////////////////
  // data part
  // FooterItem has 3 states: Loading, Error, NoMore
  public final static int LOADING = 0;
  public final static int ERROR = 1;
  public final static int NO_MORE = 2;
  private int state = LOADING;

  public boolean isLoading() {
    return state == LOADING;
  }

  public boolean isError() {
    return state == ERROR;
  }

  public boolean isNoMore() {
    return state == NO_MORE;
  }
}